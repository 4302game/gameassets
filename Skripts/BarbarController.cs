﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarbarController : MonoBehaviour {

    public AnimationClip idle;
    public AnimationClip attak;
    private Animation animation1;
    public bool attaka;

	// Use this for initialization
	void Start () {
        animation1 = gameObject.GetComponent<Animation>();
        animation1.CrossFade(idle.name);
    }
	
	// Update is called once per frame
	void Update () {
        if(!attaka)
            animation1.CrossFade(idle.name);
        else
            animation1.CrossFade(attak.name);
    }
}
