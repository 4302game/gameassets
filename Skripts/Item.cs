﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {

    public string sprite;
    public string prefab;
    public int inventoryPlace = -1;

    public Item(string _sprite, string _prefab, int _invPlace)
    {
        sprite = _sprite;
        prefab = _prefab;
        inventoryPlace = _invPlace;
    }
	// Use this for initialization
	//void Start () {
		
	//}
	
	//// Update is called once per frame
	//void Update () {
		
	//}
}
