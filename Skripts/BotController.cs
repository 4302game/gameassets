﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class BotController : MonoBehaviour
{
    public bool watchToPlayer;
    public GameObject player;
    public Animator animator;
    public int health;
    private GameObject miniBar;
    private GameObject bigBar;
    private Interface interfaceClass;
    private Vector3 barScale;
    private float distance;
    private NavMeshAgent agent;
    private bool activ;

    // Use this for initialization
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        watchToPlayer = true;
        health = 100;
        player = GameObject.FindWithTag("Player");
        animator.SetBool("Walk", true);
        interfaceClass = player.gameObject.GetComponent<Interface>();
        bigBar = interfaceClass.interface1.transform.GetChild(2).gameObject;
        miniBar = interfaceClass.interface1.transform.GetChild(3).gameObject;
        barScale = miniBar.transform.localScale;
        agent = gameObject.GetComponent<NavMeshAgent>();
        activ = false;

        //player = player.transform.GetChild(0).gameObject;

    }

    // Update is called once per frame
    void Update()
    {
        //animator.SetBool("Grounded", true);
        if (health <= 0 && watchToPlayer)
        {
            StartCoroutine(inst());
            watchToPlayer = false;
            gameObject.GetComponent<Collider>().isTrigger = true;
            gameObject.GetComponent<Rigidbody>().useGravity = false;
            animator.SetBool("Dead", true);
            Destroy(gameObject, 3f);
        }

        if (player != null)
        {
            distance = Vector3.Distance(transform.position, player.transform.position);
            if (Input.GetKeyUp(KeyCode.X))
            {
                if (distance < 50f)
                {
                    activ = interfaceClass.targetActive;
                    Quaternion look = Quaternion.LookRotation(transform.position - Camera.main.transform.position);
                    float angle = Quaternion.Angle(Camera.main.transform.rotation, look);
                    //if (angle < 70f)
                        StartCoroutine(inst());
                }
            }
            if (Input.GetMouseButtonUp(1))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    //agent.destination = hit.point;
                    agent.destination = player.transform.position;
                }
            }
            if (interfaceClass.targetActive && interfaceClass.nameTarget == gameObject.name)
            {
                miniBar.transform.GetChild(0).gameObject.GetComponent<Slider>().value = health;

                if (distance < 50f)
                {
                    Quaternion look = Quaternion.LookRotation(transform.position - Camera.main.transform.position);
                    float angle = Quaternion.Angle(Camera.main.transform.rotation, look);
                    if (angle < 70f)
                    {
                        Vector3 screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position + Vector3.up * 2);
                        miniBar.SetActive(true);
                        bigBar.SetActive(true);
                        miniBar.transform.position = screenPoint;
                        if (distance > 20f)
                            miniBar.transform.localScale = barScale / (distance / 10f);
                        else
                            miniBar.transform.localScale = barScale / 2f;
                    }
                    else if (miniBar.activeSelf)
                    {
                        miniBar.SetActive(false);
                        bigBar.SetActive(false);
                    }
                }
                else if (miniBar.activeSelf)
                {
                    miniBar.SetActive(false);
                    bigBar.SetActive(false);
                }
            }
            if (!interfaceClass.targetActive)
            {
                miniBar.SetActive(false);
                bigBar.SetActive(false);
            }
        }
    }

    public void GetDamage(int damageFromOutside)
    {
        if (!animator.GetBool("GetDamage"))
            animator.SetBool("GetDamage", true);
        health -= damageFromOutside;
        StartCoroutine(StopHit());
    }

    void OnAnimatorIK()
    {
        if (watchToPlayer)
            animator.SetLookAtWeight(.75f);
        else
            animator.SetLookAtWeight(0);
        animator.SetLookAtPosition(player.transform.position + Vector3.up * 1.5f);
    }

    IEnumerator inst()
    {
        if (interfaceClass.targetActive)
        {
            interfaceClass.targetActive = false;
        }
        if (health > 0)
        {
            yield return new WaitForSeconds(distance / 1000f);
            if (interfaceClass.targetActive && interfaceClass.nameTarget == gameObject.name)
            {
                interfaceClass.targetActive = false;
            }
            else if (!interfaceClass.targetActive && interfaceClass.nameTarget != gameObject.name)
            {
                interfaceClass.targetActive = true;
                interfaceClass.nameTarget = gameObject.name;
            }
            else if (!activ && !interfaceClass.targetActive && interfaceClass.nameTarget == gameObject.name)
            {
                yield return new WaitForSeconds(.05f);
                if (!interfaceClass.targetActive)
                {
                    interfaceClass.targetActive = true;
                    interfaceClass.nameTarget = gameObject.name;
                }
            }
        }
    }

    IEnumerator StopHit()
    {
        yield return new WaitForSeconds(.4f);
        if (animator.GetBool("GetDamage"))
        {
            //weaponCollider.enabled = false;
            animator.SetBool("GetDamage", false);
        }
    }
}
