﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyController : MonoBehaviour {

    /*private enum ControlMode
    {
        Tank,
        Direct
    }*/

    [SerializeField] private float m_moveSpeed = 2;
    [SerializeField] private float m_turnSpeed = 200;
    [SerializeField] private float m_jumpForce = 4;
    private GameObject player;
    private Animator m_animator;
    public Transform virtualCam;
    //private Rigidbody m_rigidBody;

    //[SerializeField] private ControlMode m_controlMode = ControlMode.Direct;

    private float m_currentV = 0;
    private float m_currentH = 0;

    public float m_animSpeed = 0f;
    private float m_animIncrement = .1f;
    private readonly float m_interpolation = 10;
    private readonly float m_walkSpeed = 1f;
    private readonly float m_runSpeed = 2.5f;
    private readonly float m_frunSpeed = 4f;
    //private readonly float m_backwardsWalkScale = 0.16f;
    //private readonly float m_backwardRunScale = 0.66f;

    public Vector3 m_currentDirection = Vector3.zero;

    //private float m_jumpTimeStamp = 0;
    //private float m_minJumpInterval = 0.25f;

    //private List<Collider> m_collisions = new List<Collider>();
    public Collider weaponCollider;
    
    // Camera script
    public float speedCam = 1;
    private float _x;
    private float _y;

    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        m_animator = player.GetComponent<Animator>();
        //m_rigidBody = Player.GetComponent<Rigidbody>();

    }
	
	// Update is called once per frame
	void Update (){

        VirtualCameraRotate();

        if (Input.GetMouseButtonUp(0) && !m_animator.GetBool("Attack"))
        {
            m_animator.SetBool("Attack", true);
            weaponCollider.enabled = true;
            StartCoroutine(StopAttack());
        }

        Transform camera = Camera.main.transform;
        player.transform.eulerAngles = new Vector3(0, player.transform.eulerAngles.y, 0);

        float v = Input.GetAxis("Vertical");
        float h = Input.GetAxis("Horizontal");
        if ( v * h != 0)
        {
            Vector2 vect = new Vector2(v, h).normalized;
            v = vect.x;
            h = vect.y;
        }
        if (m_animator.GetBool("Attack")) v = h = 0;
        float inc = 0f;
        m_animSpeed = m_animator.GetFloat("MoveSpeed");

        if (Input.GetKey(KeyCode.LeftControl))
        {
            v *= m_walkSpeed;
            h *= m_walkSpeed;
            m_moveSpeed = m_walkSpeed;
            if (m_animSpeed > .05f) inc = -Mathf.Abs(m_animIncrement);
            m_animSpeed += inc;
        }
        else if (Input.GetKey(KeyCode.LeftShift))
        {
            v *= m_frunSpeed;
            h *= m_frunSpeed;
            m_moveSpeed = m_frunSpeed;
            if (m_animSpeed < 0.95f) inc = Mathf.Abs(m_animIncrement);
            m_animSpeed += inc;
        }
        else
        {
            v *= m_runSpeed;
            h *= m_runSpeed;
            m_moveSpeed = m_runSpeed;
            if (m_animSpeed > .55f)
            {
                inc = -Mathf.Abs(m_animIncrement);
            }
            else if (m_animSpeed < .45f)
            {
                inc = Mathf.Abs(m_animIncrement);
            }
            else
            {
                inc = 0;
            }
            m_animSpeed += inc;
        }

        m_currentV = Mathf.Lerp(m_currentV, v, Time.deltaTime * m_interpolation);
        m_currentH = Mathf.Lerp(m_currentH, h, Time.deltaTime * m_interpolation);

        Vector3 direction = camera.forward * m_currentV + camera.right * m_currentH;

        float directionLength = direction.magnitude;
        direction.y = 0;
        direction = direction.normalized * directionLength;

        if (direction.sqrMagnitude > 0.03)
        {
            m_currentDirection = Vector3.Slerp(m_currentDirection, direction, Time.deltaTime * m_interpolation);

            transform.rotation = Quaternion.LookRotation(m_currentDirection);
            transform.position += m_currentDirection * /*m_moveSpeed*/1.3f * Time.deltaTime;

            m_animator.SetBool("Walk", true);
            m_animator.SetFloat("MoveSpeed", m_animSpeed/*direction.magnitude*/);
        }
        else
        {
            m_animator.SetBool("Walk", false);
        }
    }

    void VirtualCameraRotate()
    {
        // Получение значения сдвига мыши
        _x = Input.GetAxis("Mouse X") * speedCam * 10;
        _y = Input.GetAxis("Mouse Y") * -speedCam * 10;

        // Вращение
        //transform.position = player.transform.position + Vector3.up * 2 - (player.transform.position + Vector3.up * 2 - transform.position).normalized * 3.8f;
        virtualCam.RotateAround(player.transform.position + Vector3.up * 2f, virtualCam.up, _x * Time.deltaTime);
        virtualCam.RotateAround(player.transform.position + Vector3.up * 2f, virtualCam.right, _y * Time.deltaTime);

        virtualCam.eulerAngles = new Vector3(virtualCam.transform.eulerAngles.x, virtualCam.transform.eulerAngles.y, 0);
    }

    IEnumerator StopAttack()
    {
        yield return new WaitForSeconds(1f);
        if (m_animator.GetBool("Attack"))
        {
            weaponCollider.enabled = false;
            m_animator.SetBool("Attack", false);
        }
    }
}
