﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponInAttack : MonoBehaviour {

    //Collision
    public GameObject target;
    public int damage;

    // Use this for initialization
    void Start () {
        target = null;

    }
	
	// Update is called once per frame
	void Update () {
    }

    private void OnTriggerEnter(Collider collider)
    {
        target = collider.gameObject;
        if (target.tag == "Player" || target.tag == "Bot")
            target.gameObject.BroadcastMessage("GetDamage", damage);
        target = null;
    }
}
