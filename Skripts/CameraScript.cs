﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    private GameObject player;
    private Transform playerPos;
    private Transform vCamPos;

    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        playerPos = player.transform;
        vCamPos = player.GetComponent<MyController>().virtualCam;
    }


    void LateUpdate()
    {
        // Вращение
        transform.position = playerPos.position + vCamPos.localPosition;
        transform.rotation = Quaternion.LookRotation(playerPos.position + Vector3.up * 2f - transform.position);

        // Приближение/удаление
        /*if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            _distance = Vector3.Distance(transform.position, player.transform.position);
            if (Input.GetAxis("Mouse ScrollWheel") > 0 && _distance > minDistance)
            {
                transform.Translate(Vector3.forward * Time.deltaTime * speedScroll);
            }

            if (Input.GetAxis("Mouse ScrollWheel") < 0 && _distance < maxDistance)
            {
                transform.Translate(Vector3.forward * Time.deltaTime * -speedScroll);
            }
        }*/
    }
}
