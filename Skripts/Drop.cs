﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class Drop : MonoBehaviour, IDropHandler {

    public int numCell;

    public void Start()
    {
        string str = gameObject.name.Trim(new char[] { ')', '(' }); // результат "ello worl"
        numCell = Int32.Parse(str.Remove(0, 6));
    }

    public void OnDrop(PointerEventData eventData)
    {
        Drag drag = eventData.pointerDrag.GetComponent< Drag>();
        if(drag != null)
        {
            if(transform.childCount > 0)
            {
                Drag dragold = transform.GetChild(0).gameObject.GetComponent <Drag>();
                dragold.item.inventoryPlace = drag.item.inventoryPlace;
                transform.GetChild(0).SetParent(drag.old);
                //drag.item.inventoryPlace = numCell;
            }
            drag.transform.SetParent(transform);
            drag.item.inventoryPlace = numCell;
        }
    }

 //   // Use this for initialization
 //   void Start () {
		
	//}
	
	//// Update is called once per frame
	//void Update () {
		
	//}
}
