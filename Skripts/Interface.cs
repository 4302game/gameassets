﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interface : MonoBehaviour {

    private List<Item> list;
    public GameObject inventory;
    public GameObject bigMap;
    public GameObject container;
    public GameObject interface1;
    private int inventoryCount;
    private bool[] inv;

    public string nameTarget;
    public bool targetActive;

	// Use this for initialization
	void Start () {
        interface1.SetActive(true);
        list = new List<Item>();
        inventoryCount = 12;
        inv = new bool[inventoryCount];
        for (int i = 0; i < inv.Length; i++)
        {
            inv[i] = false;
        }
        targetActive = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonUp(1))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit))
            {
                Item item = hit.collider.GetComponent<Item>();
                if (item != null)
                {
                    for (int i = 0; i < inv.Length; i++)
                    {
                        inv[i] = false;
                    }
                    foreach (Item it in list)
                    {
                        inv[it.inventoryPlace] = true;
                    }
                    for (int i = 0; i < inventoryCount; i++)
                        if (!inv[i])
                        {
                            item.inventoryPlace = i;
                            break;
                        }
                    list.Add(item);
                    Destroy(hit.collider.gameObject);
                    if (inventory.activeSelf)
                    {
                        if (list.Count <= inventoryCount)
                        {
                            GameObject img = Instantiate(container);
                            if (item.inventoryPlace != -1) img.transform.SetParent(inventory.transform.GetChild(item.inventoryPlace));
                            else
                                for (int j = 0; j < inventoryCount; j++)
                                    if (inventory.transform.GetChild(j).childCount == 0)
                                    {
                                        img.transform.SetParent(inventory.transform.GetChild(3));
                                        break;
                                    }
                            img.GetComponent<Image>().sprite = Resources.Load<Sprite>(item.sprite);
                            img.GetComponent<Drag>().item = item;
                        }
                    }
                }
            }
        }
        if (Input.GetKeyUp(KeyCode.I))
        {
            if (inventory.activeSelf)
            {
                inventory.SetActive(false);
                for (int i = 0; i < inventory.transform.childCount; i++)
                {
                    if(inventory.transform.GetChild(i).transform.childCount > 0)
                    {
                        Destroy(inventory.transform.GetChild(i).transform.GetChild(0).gameObject);
                    }
                }
            }
            else
            {
                inventory.SetActive(true);
                int count = list.Count;
                for (int i = 0; i< count; i++)
                {
                    Item it = list[i];
                    if (inventory.transform.childCount >= i)
                    {
                        GameObject img = Instantiate(container);
                        if (it.inventoryPlace != -1) img.transform.SetParent(inventory.transform.GetChild(it.inventoryPlace));
                        else
                            for (int j = 0; j < inventoryCount; j++) 
                                if (inventory.transform.GetChild(j).childCount == 0) { 
                                    img.transform.SetParent(inventory.transform.GetChild(3));
                                    break; }
                        img.GetComponent<Image>().sprite = Resources.Load<Sprite>(it.sprite);
                        img.GetComponent<Drag>().item = it;
                    }
                    else
                        break;
                }
            }
        }
        if (Input.GetKeyUp(KeyCode.M))
        {
            bigMap.SetActive(!bigMap.activeSelf);
        }
        if (Input.GetKeyUp(KeyCode.V))
        {
            interface1.SetActive(!interface1.activeSelf);
        }
    }

    public void Remove(Drag drag)
    {
        Item it = drag.item;
        GameObject newo = Instantiate<GameObject>(Resources.Load<GameObject>(it.prefab));
        newo.transform.position = transform.position + transform.forward + transform.up;
        Destroy(drag.gameObject);
        list.Remove(it);
    }
}
